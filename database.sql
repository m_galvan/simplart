DROP DATABASE IF EXISTS simplart;
CREATE DATABASE simplart;
USE simplart;
DROP TABLE IF EXISTS artist;
CREATE TABLE artist (
  id INTEGER PRIMARY KEY AUTO_INCREMENT,
  first_name VARCHAR(50) NOT NULL,
  last_name VARCHAR(50) NOT NULL,
  email VARCHAR(50) NOT NULL,
  password VARCHAR(255) NOT NULL,
  role VARCHAR(50) NOT NULL,
  created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
  updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);
DROP TABLE IF EXISTS user;
CREATE TABLE user (
  id INTEGER PRIMARY KEY AUTO_INCREMENT,
  first_name VARCHAR(50) NOT NULL,
  last_name VARCHAR(50) NOT NULL,
  email VARCHAR(50) NOT NULL,
  password VARCHAR(255) NOT NULL,
  role VARCHAR(50) NOT NULL,
  created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
  updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);
DROP TABLE IF EXISTS category;
CREATE TABLE category (
  id INTEGER PRIMARY KEY AUTO_INCREMENT,
  name VARCHAR(50) NOT NULL
);
DROP TABLE IF EXISTS painting;
CREATE TABLE painting (
  id INTEGER PRIMARY KEY AUTO_INCREMENT,
  title VARCHAR(50) NOT NULL,
  description VARCHAR(50),
  image VARCHAR(255),
  created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  category_id INTEGER,
  artist_id INTEGER,
  FOREIGN KEY (category_id) REFERENCES category (id),
  FOREIGN KEY (artist_id) REFERENCES artist (id)
);

INSERT INTO
  user (first_name, last_name, email, password, role)
VALUES
  (
    'Nicolas',
    'Large',
    'nico@mail.fr',
    '$2a$10$dVENM9brYsZ4QQn9ALwTwevvxhRwuLBRZLBZ3k/P3jrhz5lcBbY6i',
    'ROLE_USER'
  );

  INSERT INTO
  artist (first_name, last_name, email, password, role)
VALUES
  (
    'Fred',
    'Dupont',
    'fred@mail.fr',
    '$2a$10$dVENM9brYsZ4QQn9ALwTwevvxhRwuLBRZLBZ3k/P3jrhz5lcBbY6i',
    'ROLE_ARTIST'
  );

  INSERT INTO
  user (first_name, last_name, email, password, role)
VALUES
  (
    'Melissa',
    'Galvan',
    'mel@mail.fr',
    '$2a$10$dVENM9brYsZ4QQn9ALwTwevvxhRwuLBRZLBZ3k/P3jrhz5lcBbY6i',
    'ROLE_ADMIN'
  );
