package dwjee.melissa.simplart.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Repository;

import dwjee.melissa.simplart.entity.Artist;

@Repository
public class ArtistRepository implements UserDetailsService {
    private static final String GET_ARTISTS = "SELECT * FROM artist";
    private static final String ADD_ARTIST = "INSERT INTO artist (first_name, last_name, email, password, role) VALUES (?,?,?,?,?)";
    private static final String GET_BY_EMAIL = "SELECT * FROM artist WHERE email=?";
    private static final String GET_BY_ID = "SELECT * FROM artist WHERE id=?";

    @Autowired
    private DataSource dataSource;

    /**
     * To find all artists.
     * @return
     */
    public List<Artist> findAll() {
        Connection cnx;
        List<Artist> list = new ArrayList<>();
        try {
            cnx = dataSource.getConnection();
            PreparedStatement stmt = cnx.prepareStatement(GET_ARTISTS);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                list.add(new Artist(rs.getInt("id"), rs.getString("first_name"), rs.getString("last_name"),
                        rs.getString("email"), rs.getString("password"), rs.getDate("created_at"),
                        rs.getDate("updated_at"), rs.getString("role")));
            }
            cnx.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;

    }

    /**
     * To save an artist.
     * @param artist
     * @return
     */
    public boolean save(Artist artist) {
        Connection cnx;
        try {
            cnx = dataSource.getConnection();
            PreparedStatement stmt = cnx
                    .prepareStatement(ADD_ARTIST,
                            PreparedStatement.RETURN_GENERATED_KEYS);

            stmt.setString(1, artist.getFirstName());
            stmt.setString(2, artist.getLastName());
            stmt.setString(3, artist.getEmail());
            stmt.setString(4, artist.getPassword());
            stmt.setString(5, artist.getRole());

            if (stmt.executeUpdate() == 1) {
                ResultSet result = stmt.getGeneratedKeys();
                result.next();
                artist.setId(result.getInt(1));

                return true;
            }
            cnx.close();
        } catch (SQLException e) {

            e.printStackTrace();
        }

        return false;
    }

    /**
     * To find an artist by email.
     * @param email
     * @return
     */
    public Artist findByEmail(String email) {
        Connection cnx;
        try {
            cnx = dataSource.getConnection();
            PreparedStatement stmt = cnx
                    .prepareStatement(GET_BY_EMAIL);
            stmt.setString(1, email);
            ResultSet result = stmt.executeQuery();
            if (result.next()) {
                return new Artist(
                    result.getInt("id"),
                    result.getString("first_name"),
                    result.getString("last_name"),
                    result.getString("email"),
                    result.getString("password"),
                    result.getString("role"));
            }
            cnx.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * To find an artist by id.
     * @param id
     * @return
     */
    public Artist findById(int id) {
        Connection cnx;
        try {
            cnx = dataSource.getConnection();
            PreparedStatement stmt = cnx
                    .prepareStatement(GET_BY_ID);
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();
            if (result.next()) {
                return new Artist(
                        result.getInt("id"),
                        result.getString("first_name"),
                        result.getString("last_name"),
                        result.getString("email"),
                        result.getString("password"),
                        result.getString("role"));
            }
            cnx.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Artist artist = findByEmail(username);
        if (artist == null) {
            throw new UsernameNotFoundException("Artiste introuvable.");
        }
        return artist;
    }

    /**
     * Delete artist by id.
     * @param id
     * @return
     */
    public boolean deleteById(Integer id) {
        Connection cnx;
        try {
            cnx = dataSource.getConnection();
            PreparedStatement stmt = cnx.prepareStatement("DELETE FROM artist WHERE id=?");
            stmt.setInt(1, id);
            return stmt.executeUpdate() == 1;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
}
