package dwjee.melissa.simplart.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import dwjee.melissa.simplart.repository.UserRepository;

@Controller
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserRepository userRepo;

    /**
     * Return user account.
     * @param model
     * @return
     */
    @GetMapping("/account")
    public String getAll(Model model) {
        model.addAttribute("users", userRepo.findAll());
        return "user-account";
    }
}
