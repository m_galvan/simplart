package dwjee.melissa.simplart.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import dwjee.melissa.simplart.repository.ArtistRepository;
import dwjee.melissa.simplart.repository.UserRepository;

@Controller
@RequestMapping("/admin")
public class AdminController {
    @Autowired
    UserRepository userRepository;
    @Autowired
    ArtistRepository artistRepository;

    /**
     * Return global manage page.
     *
     * @return
     */
    @GetMapping("/account")
    public String getManagePage() {
        return "admin/manage";
    }

    /**
     * Return page to manage artists
     *
     * @param model
     * @return
     */
    @GetMapping("/manage-artists")
    public String getAllArtists(Model model) {
        model.addAttribute("artists", artistRepository.findAll());
        return "admin/manage-artists";
    }

    /**
     * Return page to manage users.
     *
     * @param model
     * @return
     */
    @GetMapping("/manage-users")
    public String getAllUsers(Model model) {
        model.addAttribute("users", userRepository.findAll());
        return "admin/manage-users";
    }

    /**
     * Delete user by id.
     *
     * @param id
     * @return
     */
    @GetMapping("/delete-user/{id}")
    public String deleteUser(@PathVariable("id") int id) {
        userRepository.deleteById(id);
        return "redirect:/admin/manage-users";
    }

    /**
     * Delete artist by id.
     *
     * @param id
     * @return
     */
    @GetMapping("/delete-artist/{id}")
    public String deleteArtist(@PathVariable("id") int id) {
        artistRepository.deleteById(id);
        return "redirect:/admin/manage-artists";
    }
}
