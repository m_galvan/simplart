package dwjee.melissa.simplart.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import dwjee.melissa.simplart.repository.CategoryRepository;

@Controller
public class CategoryController {
    @Autowired
    private CategoryRepository categoryRepo;

    @GetMapping("/category/{id}")
    public String getAllPaintingbyCategory(@PathVariable("id") int id, Model model) {
        model.addAttribute("category", categoryRepo.findById(id));
        model.addAttribute("paintings", categoryRepo.findByCategory(id));
        return "category/one-category";
    }

}
